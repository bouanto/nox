(*
 * Copyright (C) 2018  Boucher, Antoni <bouanto@zoho.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *)

type file_position = {
    position_column: int;
    position_filename: string;
    position_line: int;
}

type file = {
    channel: in_channel;
    stream: char Stream.t;
}

let string_of_file_pos pos =
    "line " ^ string_of_int pos.position_line ^ ", column " ^ string_of_int pos.position_column

let complete_string_of_file_pos pos =
    pos.position_filename ^ ":" ^ string_of_int pos.position_line ^ ":" ^ string_of_int pos.position_column ^ ": "

let eof = char_of_int 4

class reader filename =
    object (self)
        val mutable file_column = 0
        val mutable file_line = 1
        val file = (
            let channel = open_in filename in
            let stream = Stream.of_channel channel in
            {
                channel;
                stream;
            }
        )

        method close =
            close_in file.channel

        method peek_char =
            match Stream.peek file.stream with
            | Some char -> char
            | None -> raise End_of_file

        method peek_2char =
            match Stream.npeek 2 file.stream with
            | [_; _] as chars ->  String.concat "" (List.map (String.make 1) chars)
            | _ -> ""

        method peek_next_char =
            match Stream.npeek 2 file.stream with
            | [_; char] -> char
            | _ -> raise End_of_file

        method private get_current_char =
            match Stream.peek file.stream with
            | Some char -> char
            | None -> eof

        method private get_next_char_noerr =
            match Stream.npeek 2 file.stream with
            | [_; char] -> char
            | _ -> eof

        method next_char =
            Stream.junk file.stream;
            if self#get_current_char = '\n' || (self#get_current_char = '\r' && self#get_next_char_noerr <> '\n') then
                self#next_line
            else
                self#next_column

        method private next_column =
            file_column <- file_column + 1

        method private next_line =
            file_column <- 0;
            file_line <- file_line + 1

        method position = {
            position_column = file_column;
            position_filename = filename;
            position_line = file_line;
        }
    end
