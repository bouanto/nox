(*
 * Copyright (C) 2018  Boucher, Antoni <bouanto@zoho.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *)

open Filename

open Llvm
open Llvm_scalar_opts
open Llvm_target

open Ast

exception Error of string

type manager = {
    modul: llmodule;
    pass_manager: [`Function] PassManager.t;
}

let double_type = double_type (global_context ())
let int_type = i64_type (global_context ())

class generator ast =
    object (self)
        val context = global_context ()
        val manager = (
            let modul = create_module (global_context ()) "jit" in
            {
                modul;
                pass_manager = PassManager.create_function modul;
            }
        )
        val builder = builder (global_context ())
        val named_values: (string, llvalue) Hashtbl.t = Hashtbl.create 10
        val target_machine = (
            Llvm_all_backends.initialize ();
            let target_triple = Target.default_triple () in
            let target = Target.by_triple target_triple in
            let cpu = "generic" in
            let features = "" in
            let opt = CodeGenOptLevel.Default in
            let reloc_mode = RelocMode.Default in
            let code_model = CodeModel.Default in
            TargetMachine.create ~triple:target_triple ~cpu ~features ~level:opt ~reloc_mode ~code_model target
        )

        initializer
            let target_triple = Target.default_triple () in
            set_data_layout (DataLayout.as_string (TargetMachine.data_layout target_machine)) manager.modul;
            set_target_triple target_triple manager.modul;

            ignore (Llvm_executionengine.initialize ());
            add_instruction_combination manager.pass_manager;
            add_reassociation manager.pass_manager;
            add_gvn manager.pass_manager;
            add_cfg_simplification manager.pass_manager;
            add_memory_to_register_promotion manager.pass_manager;
            ignore (PassManager.initialize manager.pass_manager);

        method private create_entry_block_alloca func var_name =
            let builder = builder_at context (instr_begin (entry_block func)) in
            build_alloca double_type var_name builder

        method private create_argument_allocas func proto =
            let params =
                match proto with
                | Ast.Prototype {params} | Ast.BinOpPrototype {params} -> params
            in
            Array.iteri (fun i arg ->
                let var_name = params.(i) in
                let alloca = self#create_entry_block_alloca func var_name in
                ignore (build_store arg alloca builder);
                Hashtbl.add named_values var_name alloca;
            ) (Llvm.params func)

        method private expr = function
            | Ast.Float num -> const_float double_type num
            | Ast.Variable name ->
                let var =
                    try
                        Hashtbl.find named_values name
                    with
                    | Not_found -> raise (Error "unknown variable name")
                in
                build_load var name builder
            | Ast.Binary {op; left; right} ->
                (*if op = '=' then
                    let name =
                        match left with
                        | Ast.Variable name -> name
                        | _ -> raise (Stream.Error "destination of '=' must be a variable")
                    in
                    let value = self#expr right in
                    let variable =
                        try
                            Hashtbl.find named_values name
                        with
                        | Not_found -> raise (Stream.Error "Unknown variable name")
                    in
                    ignore (build_store value variable builder);
                    value
                else *) (
                    let left_value = self#expr left in
                    let right_value = self#expr right in
                    match op with
                    | Ast.Plus -> build_fadd left_value right_value "addtmp" builder
                    | Ast.Minus -> build_fsub left_value right_value "subtmp" builder
                    | Ast.Times -> build_fmul left_value right_value "multmp" builder
                    | Ast.LesserThan ->
                        let bool = build_fcmp Fcmp.Ult left_value right_value "cmptmp" builder in
                        build_uitofp bool double_type "booltmp" builder
                    | _ ->
                        failwith "unimplemented call to custom binary operator"
                        (*let callee = "binary" ^ (String.make 1 op) in
                        let callee =
                            match lookup_function callee manager.modul with
                            | Some callee -> callee
                            | None -> raise (Error "binary operator not found")
                        in
                        build_call callee [|left_value; right_value|] "binop" builder*)
                )
            | Ast.Call {func_name; args} ->
                let func_name =
                    match lookup_function func_name manager.modul with
                    | Some func_name -> func_name
                    | None -> raise (Error ("unknown function referenced: " ^ func_name))
                in
                let params = params func_name in
                if Array.length params = Array.length args then
                    ()
                else
                    raise (Error "incorrect # arguments passed");
                let args = Array.map self#expr args in
                build_call func_name args "calltmp" builder
            | Ast.If {cond; if_expr; else_expr} ->
                let cond = self#expr cond in
                let zero = const_float double_type 0.0 in
                let cond_val = build_fcmp Fcmp.One cond zero "ifcond" builder in
                let start_block = insertion_block builder in
                let func = block_parent start_block in
                let then_block = append_block context "then" func in
                position_at_end then_block builder;
                let then_val = self#expr if_expr in
                let new_then_block = insertion_block builder in
                let else_block = append_block context "else" func in
                position_at_end else_block builder;
                let else_val = self#expr else_expr in
                let new_else_block = insertion_block builder in
                let merge_block = append_block context "ifcont" func in
                position_at_end merge_block builder;
                let incoming = [(then_val, new_then_block); (else_val, new_else_block)] in
                let phi = build_phi incoming "iftmp" builder in
                position_at_end start_block builder;
                ignore (build_cond_br cond_val then_block else_block builder);
                position_at_end new_then_block builder;
                ignore (build_br merge_block builder);
                position_at_end new_else_block builder;
                ignore (build_br merge_block builder);
                position_at_end merge_block builder;
                phi
            | Ast.For (var_name, start, end_var, step, body) ->
                let func = block_parent (insertion_block builder) in
                let alloca = self#create_entry_block_alloca func var_name in
                let start_val = self#expr start in
                ignore (build_store start_val alloca builder);
                let loop_block = append_block context "loop" func in
                ignore (build_br loop_block builder);
                position_at_end loop_block builder;
                let old_val =
                    try
                        Some (Hashtbl.find named_values var_name)
                    with Not_found -> None
                in
                Hashtbl.add named_values var_name alloca;
                ignore (self#expr body);
                let step_val =
                    match step with
                    | Some step -> self#expr step
                    | None -> const_float double_type 1.0
                in
                let end_condition = self#expr end_var in
                let current_val = build_load alloca var_name builder in
                let next_val = build_fadd current_val step_val "nextvar" builder in
                ignore (build_store next_val alloca builder);
                let zero = const_float double_type 0.0 in
                let end_condition = build_fcmp Fcmp.One end_condition zero "loopcond" builder in
                let after_block = append_block context "afterloop" func in
                ignore (build_cond_br end_condition loop_block after_block builder);
                position_at_end after_block builder;
                (match old_val with
                | Some old_val -> Hashtbl.add named_values var_name old_val
                | None -> ()
                );
                const_null double_type
            | Ast.Sequence exprs ->
                List.fold_left (fun _ expr -> self#expr expr) self#null exprs
            | Ast.Unary {op; expr} ->
                raise (Stream.Error "unary operator unimplemented")
            | Ast.Var {var_expr; var_name; body} ->
                (* TODO: use a proper environment *)
                let old_bindings = ref [] in
                let func = block_parent (insertion_block builder) in
                let alloca = self#create_entry_block_alloca func var_name in
                let expr = self#expr var_expr in
                ignore (build_store expr alloca builder);
                (try
                    let old_value = Hashtbl.find named_values var_name in
                    old_bindings := (var_name, old_value) :: !old_bindings
                with Not_found -> ()
                );
                Hashtbl.add named_values var_name alloca;
                let body_val = self#expr body in
                List.iter (fun (var_name, old_value) ->
                    Hashtbl.add named_values var_name old_value
                ) !old_bindings;
                body_val

        method private proto = function
            | Ast.Prototype {func_name; params} | Ast.BinOpPrototype {func_name; params} ->
                let doubles = Array.make (Array.length params) double_type in
                let ft = function_type double_type doubles in
                let func =
                    match lookup_function func_name manager.modul with
                    | None -> declare_function func_name ft manager.modul
                    | Some func ->
                        if Array.length (basic_blocks func) == 0 then
                            ()
                        else
                            raise (Error "redefinition of function");

                        if Array.length (Llvm.params func) == Array.length params then
                            ()
                        else
                            raise (Error "redefinition of function with different # args");
                        func
                in
                Array.iteri (fun i a ->
                    let n = params.(i) in
                    set_value_name n a;
                    Hashtbl.add named_values n a;
                ) (Llvm.params func);
                func

        method private func = function
            | Ast.Function {proto; body} ->
                Hashtbl.clear named_values;
                let func = self#proto proto in
                let basic_block = append_block context "entry" func in
                position_at_end basic_block builder;
                try
                    self#create_argument_allocas func proto;
                    let return_value = self#expr body in
                    let _ = build_ret return_value builder in
                    Llvm_analysis.assert_valid_function func;
                    ignore (PassManager.run_function func manager.pass_manager);
                    func
                with error ->
                    delete_function func;
                    raise error

        method private null =
            const_int int_type 0

        method object_file =
            List.iter (fun dec ->
                match dec with
                | Ast.Def func ->
                    ignore (self#func func)
                | Ast.Extern extern ->
                    ignore (self#proto extern)
            ) ast.declarations;
            let filename = basename (remove_extension ast.filename) in
            let filename = "output/" ^ filename ^ ".o" in
            TargetMachine.emit_to_file manager.modul CodeGenFileType.ObjectFile filename target_machine;
            [filename]
    end
